export interface User {
  _id: string;
  email: string;
  first_name: string;
  last_name: string;
  password: string;
  city: string;
  country: string;
  createdAt: string;
  updatedAt: string;
}
