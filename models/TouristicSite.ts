export interface TouristicSite {
  _id: string;
  location_name: string;
  address: string;
  average_price: string;
  categories: string[];
  city: string;
  country: string;
  image: any;
  latitude: string;
  length: string;
  phone: string;
  rating: string;
  web: string;
  weight?: number
}
