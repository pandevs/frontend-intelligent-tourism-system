import React, { useState } from "react";
import styles from "./RegisterForm.module.scss";
import { FaHome } from "react-icons/fa";
import { FiUserPlus } from "react-icons/fi";
import { BiShow } from "react-icons/bi";
import { BiHide } from "react-icons/bi";
import { AiOutlineMail } from "react-icons/ai";
import { RiLockPasswordLine } from "react-icons/ri";
import { MdGpsFixed } from "react-icons/md";
import * as Yup from "yup";
import { Formik } from "formik";
import api from "../../api";
import { useRouter } from "next/router";

const RegisterForm = () => {
	const router = useRouter();
	const [show, setShow] = useState("password");
	const [registerNew, setRegisterNew] = useState(false);
	const registerSuccess =
		"felicidades tu cuenta ha sido creada, usa tus credenciales para iniciar sesión!";

	return (
		<Formik
			initialValues={{
				email: "",
				password: "",
				first_name: "",
				last_name: "",
				country: "",
				city: "",
			}}
			onSubmit={async (values, { setSubmitting }) => {
				try {
					const response = await api.auth.register(values);
					if (response.status === 201) {
						setRegisterNew(true);
						setTimeout(() => {
							router.push("/login");
						}, 4000);
					}
				} catch (error) {
					console.log(error);
				}
			}}
			validationSchema={Yup.object().shape({
				email: Yup.string()
					.email("Correo no valido")
					.required("Campo Requerido"),
				password: Yup.string()
					.required("No ha ingresado la contraseña")
					.min(8, "La contraseña debe tener 8 carácteres minimo.")
					.matches(/(?=.*[0-9])/, "La contraseña debe tener un número"),
				first_name: Yup.string().required("Ingresa tus nombres"),
				last_name: Yup.string().required("Ingresa tus apellidos"),
				country: Yup.string().required("Selecciona un pais"),
				city: Yup.string().required("Ingresa tu ciudad"),
			})}
		>
			{(props) => {
				const {
					values,
					touched,
					errors,
					isSubmitting,
					handleChange,
					handleBlur,
					handleSubmit,
				} = props;

				return (
					<div className={`${styles.RegisterForm}`}>
						{registerNew === true && (
							<div className="alert alert-success">{registerSuccess}</div>
						)}

						<form onSubmit={handleSubmit} className={styles.customRegisterForm}>
							<div
								className={
									errors.first_name && touched.first_name
										? `${styles.error} ${styles.RegisterItem}`
										: `${styles.RegisterItem}`
								}
							>
								<FiUserPlus size="2em" />
								<input
									name="first_name"
									type="text"
									placeholder="Nombres"
									value={values.first_name}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
							</div>
							{errors.first_name && touched.first_name && (
								<div className={`${styles.input_feedback}`}>
									{errors.first_name}
								</div>
							)}
							<div
								className={
									errors.last_name && touched.last_name
										? `${styles.error} ${styles.RegisterItem}`
										: `${styles.RegisterItem}`
								}
							>
								<FiUserPlus size="2em" />
								<input
									name="last_name"
									type="text"
									placeholder="Apellidos"
									value={values.last_name}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
							</div>
							{errors.last_name && touched.last_name && (
								<div className={`${styles.input_feedback}`}>
									{errors.last_name}
								</div>
							)}
							<div
								className={
									errors.email && touched.email
										? `${styles.error} ${styles.RegisterItem}`
										: `${styles.RegisterItem}`
								}
							>
								<AiOutlineMail size="2em" />
								<input
									name="email"
									type="email"
									placeholder="Email"
									value={values.email}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
							</div>
							{errors.email && touched.email && (
								<div className={`${styles.input_feedback}`}>{errors.email}</div>
							)}
							<div
								className={
									errors.country && touched.country
										? `${styles.error} ${styles.RegisterItem}`
										: `${styles.RegisterItem}`
								}
							>
								<MdGpsFixed size="2em" />
								<select
									name="country"
									value={values.country}
									onChange={handleChange}
									onBlur={handleBlur}
								>
									<option value="">Seleccione:</option>
									<option value="colombia">Colombia 🇨🇴</option>
									<option value="mexico">Mexico 🇲🇽</option>
								</select>
							</div>
							{errors.country && touched.country && (
								<div className={`${styles.input_feedback}`}>
									{errors.country}
								</div>
							)}
							<div
								className={
									errors.city && touched.city
										? `${styles.error} ${styles.RegisterItem}`
										: `${styles.RegisterItem}`
								}
							>
								<FaHome size="2em" />
								<input
									name="city"
									type="text"
									placeholder="Ciudad"
									value={values.city}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
							</div>
							{errors.city && touched.city && (
								<div className={`${styles.input_feedback}`}>{errors.city}</div>
							)}
							<div
								className={
									errors.password && touched.password
										? `${styles.error} ${styles.RegisterItem}`
										: `${styles.RegisterItem}`
								}
							>
								<RiLockPasswordLine size="2em" />
								<input
									name="password"
									placeholder="Contraseña"
									type={show}
									value={values.password}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div
									onClick={() => {
										if (show === "password") {
											setShow("text");
										} else {
											setShow("password");
										}
									}}
								>
									{show === "password" && <BiShow size="2em" />}
									{show === "text" && <BiHide size="2em" />}
								</div>
							</div>
							{errors.password && touched.password && (
								<div className={`${styles.input_feedback}`}>
									{errors.password}
								</div>
							)}

							<button
								type="submit"
								disabled={isSubmitting}
								className={`btn mt-2 ${styles.btnRegister}`}
							>
								Registrarse
							</button>
						</form>
					</div>
				);
			}}
		</Formik>
	);
};

export default RegisterForm;
