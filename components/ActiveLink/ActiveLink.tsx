import React, { Children } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

interface Props {
  href: string,
  baseClass: string,
  activeClass: string
  children: any
}

const ActiveLink = ({
  children, href, baseClass, activeClass,
} : Props) => {
  const child = Children.only(children);
  const router = useRouter();
  const linkClass = router.pathname === href ? `${baseClass} ${activeClass}` : `${baseClass}`;

  return (
    <Link href={href}>
      { React.cloneElement(child, { className: linkClass })}
    </Link>
  );
};

export default ActiveLink;
