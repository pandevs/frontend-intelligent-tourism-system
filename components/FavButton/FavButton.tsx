import React from 'react';
import { useFavsContext } from '@contexts/FavsContext';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';
import slugify from '@services/slugify';
import auth from '@services/auth';
import { TouristicSite } from '@models/TouristicSite';
import styles from './FavButton.module.scss';

interface Props {
  site: TouristicSite
}

const FavButton = ({ site }: Props) => {
  const { toggleFav, favs } = useFavsContext();

  const renderFavIcon = () => {
    if (!auth.getUser()) {
      return;
    }

    const found = favs.find((fav) => slugify(fav.location_name) === slugify(site.location_name));
    if (found) {
      return <AiFillHeart className={styles.FavButton} onClick={() => toggleFav(site)} />;
    }

    return <AiOutlineHeart className={styles.FavButton} onClick={() => toggleFav(site)} />;
  };

  return (
    <>
      { renderFavIcon() }
    </>
  );
};

export default FavButton;
