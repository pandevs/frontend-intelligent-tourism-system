import React from 'react';
import styles from './Filter.module.scss';

interface Props {
  Icon: any;
  name: string;
  toggleFilter: (filterName: string) => void;
  selected: boolean;
}

const Filter = ({
  Icon, name, toggleFilter, selected,
}: Props) => {
  const btnClass = () => {
    if (selected) {
      return `${styles.Filter} ${styles.Filter__selected}`;
    }

    return `${styles.Filter}`;
  };

  return (
    <button type="button" onClick={() => toggleFilter(name)} className={btnClass()}>

      <div className={styles.Filter__icon}>
        <Icon />
      </div>
      <p className={styles.Filter_text}>{name}</p>

    </button>

  );
};

export default Filter;
