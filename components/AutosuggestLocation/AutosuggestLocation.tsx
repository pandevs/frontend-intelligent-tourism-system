import React, { useState } from 'react';
import Autosuggest from 'react-autosuggest';
import { MdLocationOn } from 'react-icons/md';
import InputWrapper from '@components/InputWrapper/InputWrapper';
import styles from './AutosuggestLocation.module.scss';
import theme from './AutosuggestTheme.module.scss';

interface Props {
  data: any[];
  value: string;
  setValue: (location: string) => void
}

const AutosuggestLocation = ({ data = [], value, setValue }: Props) => {
  const [suggestions, setSuggestions] = useState([]);

  // Use your imagination to render suggestions.
  const renderSuggestion = (suggestion) => (
    <div>
      {suggestion.name}
    </div>
  );

  // Teach Autosuggest how to calculate suggestions for any given input value.
  const getSuggestions = (val) => {
    const inputValue = val.trim().toLowerCase().normalize('NFD');
    const inputLength = inputValue.length;

    return inputLength === 0
      ? []
      : data.filter((lang) => lang.name.toLowerCase().normalize('NFD').slice(0, inputLength) === inputValue);
  };

  // When suggestion is clicked, Autosuggest needs to populate the input
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  const getSuggestionValue = (suggestion) => suggestion.name;

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  const onSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value));
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  const onSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  const onChange = (event, { newValue }) => {
    setValue(newValue);
  };

  // Autosuggest will pass through all these props to the input.
  const inputProps = {
    placeholder: 'Ingresa un destino',
    value,
    onChange,
  };

  return (
    <InputWrapper Icon={MdLocationOn}>
      <Autosuggest
        theme={theme}
        suggestions={suggestions}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
      />
    </InputWrapper>
  );
};

export default AutosuggestLocation;
