import React from 'react';
import TuristicSite from '@components/TuristicSite/TuristicSite';
import Loading from '@components/Loading/Loading';
import { useTouristicSiteContext } from '@contexts/TouristicSiteContext';
import { TouristicSite } from 'models/TouristicSite';
import styles from './SearchResults.module.scss';

interface Props {
  touristicSites: any,
  loading: any,
  didSearch: boolean
}

const SearchResults = ({ touristicSites = [], loading, didSearch = false }: Props) => {
  const renderResults = () => {
    if (loading) {
      return (
        <div className={styles.results_empty}>
          <Loading />
        </div>
      );
    }

    if (touristicSites.length) {
      return (
        <div className={styles.results_empty}>
          <div className={styles.results_wrapper}>
            {
              touristicSites.map((site: TouristicSite, index: number) => (
                <TuristicSite
                  site={site}
                  pos={index}
                  key={site.location_name}
                />
              ))
            }
          </div>
        </div>
      );
    }

    if (didSearch) {
      return (
        <div className={styles.results_empty}>
          <h2 className={styles.results_empty_msg}>Ops, no se encontraron resultados...</h2>
        </div>
      );
    }
  };

  return (
    <>
      { renderResults()}
    </>
  );
};

export default SearchResults;
