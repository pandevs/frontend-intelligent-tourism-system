import React from 'react';
import styles from './Greeting.module.scss';
import Button from '@components/Button/Button'

interface Props {
  title: string;
  content1: string;
  content2: string;
  btnText: string;
  path: string;
}

const Greeting = ({
  title, content1, content2, btnText, path,
}: Props) => (
    <div className={styles.imagesDetail}>
      <h2>{title}</h2>
      <p>
        {content1}
        {' '}
        <br />
        {' '}
        {content2}
      </p>
      <img src="/img/logo.png" alt="logo" />
      <small className="d-block">¿Ya tienes cuenta?</small>
      <Button path={path} btnText={btnText} />
    </div>
  );

export default Greeting;
