import React from 'react';
import * as Yup from 'yup';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useUserContext } from '@contexts/UserContext';
import { AiOutlineMail } from 'react-icons/ai';
import { RiLockPasswordLine } from 'react-icons/ri';
import { Formik } from 'formik';
import authService from '@services/auth';
import api from '../../api';
import styles from './LoginForm.module.scss';

const LoginForm = () => {
  const { setUser, setToken } = useUserContext();
  const router = useRouter();

  return (
    <Formik
      initialValues={{ email: '', password: '', loginError: '' }}
      onSubmit={async ({ email, password }, { setSubmitting, setErrors }) => {
        try {
          const response = await api.auth.login(email, password);

          if (response.user) {
            setUser(response.user);
            setToken(response.token);
            authService.login(response.user, response.token);
            router.push('/');
          } else {
            setErrors({ loginError: 'Tus credenciales son incorrectas' });
          }
        } catch (error) {
          setErrors({ loginError: 'Tus credenciales son incorrectas' });
        }

        setSubmitting(false);
      }}

      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email('Correo no valido')
          .required('Campo Requerido'),
        password: Yup.string()
          .required('No ha ingresado la contraseña')
          .min(8, 'La contraseña debe tener 8 carácteres minimo.')
          .matches(/(?=.*[0-9])/, 'La contraseña debe tener un número'),
      })}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          setErrors,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;

        const closeErrorMessage = () => {
          setErrors({ loginError: '' });
        };

        return (
          <div className={`${styles.loginForm}`}>

            {errors.loginError && (
              <div className="alert alert-danger alert-dismissible fade show" role="alert">
                {errors.loginError}
                <button type="button" className="close" aria-label="Close" onClick={closeErrorMessage}>
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            )}

            <form
              onSubmit={handleSubmit}
              className={styles.customLoginForm}
            >
              <div className={(errors.email && touched.email) ? `${styles.error} ${styles.loginItem}` : `${styles.loginItem}`}>
                <AiOutlineMail size="2em" />
                <input
                  name="email"
                  type="email"
                  placeholder="Email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {errors.email && touched.email && (
                <div className={`${styles.input_feedback}`}>{errors.email}</div>
              )}
              <div className={(errors.password && touched.password) ? `${styles.error} ${styles.loginItem}` : `${styles.loginItem}`}>
                <RiLockPasswordLine size="2em" />
                <input
                  name="password"
                  placeholder="Contraseña"
                  type="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {errors.password && touched.password && (
                <div className={`${styles.input_feedback}`}>{errors.password}</div>
              )}
              <Link href="/forgotPassword">
                <a className="d-block mt-2">
                  ¿Olvidó la contraseña?
                </a>
              </Link>
              <button type="submit" disabled={isSubmitting} className={`btn mt-2 ${styles.btnLogin}`}>
                Inicia sesión
              </button>

            </form>
          </div>
        );
      }}
    </Formik>
  );
};

export default LoginForm;
