import React from 'react'
import Link from 'next/link';
import styles from './Button.module.scss'

const button = ({ path, btnText }) => (
  <Link href={path}>
    <a className={`btn ${styles.btn}`}>{btnText}</a>
  </Link>
)

export default button
