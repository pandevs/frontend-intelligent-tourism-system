import React from 'react';
import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api';
import mapStyles from './MapStyles';

const mapContainerStyle = {
  height: 'inherit',
};

const options = {
  styles: mapStyles,
  disableDefaultUI: true,
  zoomControl: true,
};

const Map = ({ lat, lng }) => {
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_MAPS_KEY,
  });

  if (loadError) return <h1>Error cargando maps</h1>;
  if (!isLoaded) return <h1>Cargando maps</h1>;

  return (
    <GoogleMap
      mapContainerStyle={mapContainerStyle}
      zoom={14}
      center={{ lat: Number(lat), lng: Number(lng) }}
      options={options}
    >
      <Marker position={{ lat: Number(lat), lng: Number(lng) }} />
    </GoogleMap>
  );
};

export default Map;
