import React, { useState } from 'react';
import { useRouter } from 'next/router';
import Filters from '@components/Filters/Filters';
import AutosuggestLocation from '@components/AutosuggestLocation/AutosuggestLocation';
import useSearchTouristicSites from '@hooks/useSearchTouristicSites';
import { useSearchContext } from '@contexts/SearchContext';
import suggestions from '@services/suggestions';
import styles from './GeneralSearch.module.scss';

const GeneralSearch = () => {
  const {
    categories, selectedCategories, toggleCategory, location, setLocation,
  } = useSearchContext();
  const [searchLocation, setSearchLocation] = useState(location);
  const [selectedCategoriesState, setSelectedCategories] = useState(selectedCategories);
  const router = useRouter();
  const [loading] = useSearchTouristicSites(searchLocation, selectedCategories, router.pathname);

  const searchData = () => {
    if (!location) {
      return;
    }
    setSelectedCategories(selectedCategoriesState);
    setSearchLocation(location);
    if (router.pathname === '/') {
      router.push('/search');
    }
  };

  return (
    <>
      <div className={styles.Search_wrapper}>
        <div className={styles.Search_wrapper_center}>
          <AutosuggestLocation value={location} setValue={setLocation} data={suggestions} />
        </div>
        <div className={styles.Search_wrapper_left}>
          <button disabled={loading} onClick={searchData} type="button" className="btn btn-secondary">Buscar</button>
        </div>
      </div>

      <Filters
        categories={categories}
        selectedCategories={selectedCategories}
        toggleCategory={toggleCategory}
      />
    </>
  );
};

export default GeneralSearch;
