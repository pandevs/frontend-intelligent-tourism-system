import React from 'react';
import Filter from '@components/Filter/Filter';
import { useSearchContext } from '@contexts/SearchContext';
import styles from './Filters.module.scss';

interface Props {
  categories: any[],
  selectedCategories: string[],
  toggleCategory: (category: string) => void
}

const Filters = ({ categories, selectedCategories, toggleCategory }: Props) => (
  <div className={styles.filters_wrapper}>
    {
      categories.map((filter) => (
        <Filter
          {...filter}
          toggleFilter={toggleCategory}
          selected={selectedCategories.includes(filter.name)}
          key={filter.name}
        />
      ))
    }

  </div>
);

export default Filters;
