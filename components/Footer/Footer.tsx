import React from "react";
import styles from "./Footer.module.scss";

const Footer = () => (
	<footer className={`${styles.footer} fixed-bottom`}>example footer</footer>
);

export default Footer;
