import React from 'react';
import styles from './Loading.module.scss';

const Loading = () => (
  <div className={styles.lds_grid}>
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </div>
);

export default Loading;
