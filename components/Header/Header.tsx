import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import ActiveLink from '@components/ActiveLink/ActiveLink';
import authService from '@services/auth';
import { useUserContext } from '@contexts/UserContext';
import styles from './Header.module.scss';

const Header = () => {
  const { user, setUser, setToken } = useUserContext();
  const router = useRouter();

  const logout = () => {
    setUser(null);
    setToken('');
    authService.logout();
    router.push('/');
  };

  const renderLinks = () => {
    if (user) {
      return (
        <>
          <li className="nav-item">
            <ActiveLink baseClass="nav-link" activeClass="active" href="/profile">
              <a>{user.first_name}</a>
            </ActiveLink>
          </li>
          <li className="nav-item">
            <ActiveLink baseClass="nav-link" activeClass="active" href="/recommendations">
              <a>Recomendaciones</a>
            </ActiveLink>
          </li>
          <li className="nav-item">
            <button type="button" onClick={logout} className={`${styles.Home__logout_btn} nav-link`}>
              <a>Cerrar sesión</a>
            </button>
          </li>
        </>
      );
    }

    if (authService.getUser()) {
      setToken(authService.getToken());
      setUser(authService.getUser());
      return;
    }

    return (
      <>
        <li className="nav-item">
          <ActiveLink baseClass="nav-link" activeClass="active" href="/login">
            <a>Iniciar sesión</a>
          </ActiveLink>
        </li>
        <li className="nav-item">
          <ActiveLink baseClass="nav-link" activeClass="active" href="/register">
            <a>Registrarse</a>
          </ActiveLink>
        </li>
      </>
    );
  };

  return (
    <nav className={`${styles.Home} navbar navbar-expand-lg navbar-dark bg-primary`}>
      <div className="container-fluid">
        <Link href="/">
          <a className="navbar-brand">
            <img className="mr-2" src="/img/logo.png" alt="logo_pandar" width="25" />
            <span className={styles.Home__logo_title}>
              Pan
              <span className={styles.Home__logo_title_accent}>dar</span>
            </span>
          </a>
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className={`${styles.Home__links} collapse navbar-collapse`} id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto mb-2 mb-lg-0">
            {renderLinks()}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;
