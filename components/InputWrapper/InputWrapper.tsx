import React from 'react';
import styles from './InputWrapper.module.scss';

interface Props {
  children: any;
  Icon: any;
}

const InputWrapper = ({ children, Icon }: Props) => (
  <div className={styles.search_wrapper}>
    <div className={styles.search_wrapper__center}>
      {children}
    </div>

    <div className={styles.search_wrapper__left}>
      <Icon size="25" />
    </div>

  </div>
);

export default InputWrapper;
