import React from 'react';
import { CgDollar } from 'react-icons/cg';
import InputWrapper from '@components/InputWrapper/InputWrapper';
import styles from './SearchInput.module.scss';

interface Props {
  value: string;
  setValue: (value: string) => void
}

const SearchInput = ({ value, setValue }: Props) => (
  <InputWrapper Icon={CgDollar}>
    <input
      value={value}
      onChange={(e) => setValue(e.target.value)}
      className={styles.search_input}
      placeholder="Presupuesto (dlrs)"
      type="number"
    />
  </InputWrapper>
);

export default SearchInput;
