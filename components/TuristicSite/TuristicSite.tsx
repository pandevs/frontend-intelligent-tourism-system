import React from "react";
import { MdLocationOn } from "react-icons/md";
import Link from "next/link";
import ReactStars from "react-rating-stars-component";
import { TouristicSite } from "models/TouristicSite";
import slugify from "@services/slugify";
import FavButton from "@components/FavButton/FavButton";
import styles from "./TuristicSite.module.scss";

interface Props {
  site: TouristicSite,
  pos: number
}

const TuristicSite = ({ site, pos }: Props) => {
  const image = (site && site.image)
    ? (site.image.medium ? site.image.medium.url : site.image)
    : 'https://static.dribbble.com/users/58267/screenshots/4257727/sans-store.jpg';

  return (
    <div className={styles.Turistic_site_wrapper}>
      <Link href="site/[name]" as={`site/${slugify(site.location_name)}`}>
        <div className={styles.Turistic_site}>
          <div className={styles.Turistic_site__img_wrapper}>
            <img className={styles.Turistic_site__img} src={image} alt="name" />
          </div>
          <div className={styles.Turistic_site__detail}>
            <h2 className="title">
              {site.location_name}
            </h2>
            <div className={styles.Turistic_site__location}>
              <MdLocationOn />
              {' '}
              {site.city}
            </div>
            <div className={styles.Turistic_site__category}>
              {site.categories?.join(', ')}
            </div>
            <div className={`${styles.Turistic_site__stars} rating`}>
              <ReactStars
                count={5}
                size={24}
                value={Number(site.rating)}
                edit={false}
                half
                activeColor="#ffd700"
              />
            </div>
          </div>
        </div>
      </Link>
      <FavButton site={site} />
      {site.weight && (
      <div className={styles.Turistic_site_badge}>
        {pos + 1}
      </div>
      )}
    </div>
  );
};

export default TuristicSite;
