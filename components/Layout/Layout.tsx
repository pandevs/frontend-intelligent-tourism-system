import React from 'react';
import Header from '@components/Header/Header';
import styles from './Layout.module.scss';

const Layout = ({ children }) => (
  <div className={styles.LayoutContainer}>
    <Header />
    {children}
  </div>
);

export default Layout;
