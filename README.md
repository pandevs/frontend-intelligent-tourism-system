# Frontend-Intelligent-Tourism-System

<div align="center">

> Demo Web App: https://pandar.vercel.app   
> Demo API: https://backend-intellegent-tourism-system.vercel.app/user  
</div>

Índice | Index
---------

[[_TOC_]]

## **Español**

### Descripción

Estructura de fondo del proyecto sistema inteligente para turistas (Pandar) que tiene como objetivo brindar las vistas y conectarse a un servicio remoto para llenar los sitios de interés para el usuario.

### Documentación

#### Tecnologías

|Tecnología                                                             |Versión        | Beneficios al Proyecto
|:----------------------------------------------------------------------|:--------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|<img src="https://img.shields.io/badge/React-blue">             |16.13.1        | Mayor dominio general en el equipo, nos permite utilizar JavaScript para construir el frontend de una aplicación web e inmensa comunidad para recibir apoyo en los proyectos |
|<img src="https://img.shields.io/badge/Next.js-black">                    |9.5.2         | Permite construir proyectos de react con una estructura estándar, lo que permite una mayor velocidad al momento de desarrollar  |
|<img src="https://img.shields.io/badge/Bootstrap-purple">              |5.0.0-alpha1          | Nos provee varios componentes ya creados en css que agilizan el maquetado del sitio |
|<img src="https://img.shields.io/badge/Typescript-darkblue">          |^3.9.7        | TypeScript nos ahorra tiempo al momento de detectar errores y provee soluciones anter de correr el código |
|<img src="https://img.shields.io/badge/Sass-pink">                  |1.26.10           | Nos permite escribir css de manera más organizada y con mayores opciones |

#### Equipo Frontend

|Miembro            |Rol                 |Tareas            |
|:------------------|:-------------------|:-----------------|
|Didier Cuetia   |**Frontend**  | Encargado del diseño, creación e integracion con el backend de las páginas 404, Registro y Login |
|Oscar Pérez    |**Frontend**         |  Encargado del diseño, creación e integracion con el backend de las páginas Home, Search y Recommendations |
|Santiago Arcila       |**Frontend**         | Encargado del diseño, creación e integracion con el backend de las páginas Recuperar contraseña, Detalle de un sitio y Favoritos |
|Todos              |**Todos**           | Pruebas manuales y creación de componentes extras |

#### Enlaces de Interés

|Nombre                      |Enlace|
|:---------------------------|:------------|
|Videos de las Reuniones     |<https://drive.google.com/drive/folders/1w8EpKRT_nDAw7tfnhyzEgh_CbkFCXN6l> |
|Diseño Base en Figma           |<https://www.figma.com/file/vL1HShMkkVo1A9IcVoVrgB/Pandar?node-id=0%3A1> |
|Repositorio del proyecto          |<https://gitlab.com/pandevs/frontend-intelligent-tourism-system> |
|Página del deploy               |<https://pandar.vercel.app> |

### Pasos para Colaborar

1. Hacer fork del proyecto: al dar click al boton fork (ubicado en la parte superior derecha)
2. Clonar el proyecto: `git clone https://gitlab.com/<Tu usuario>/backend-intellegent-tourism-system.git`
3. Instalar las dependencias: `npm install` o usar `npm i`
4. Ejecutar el proyecto: `npm run dev`
5. Realizar las mejoras necesarias en una nueva rama y hacer un commit: `git commit -am "<Mensaje del commit>"` o usar `git add .` y luego `git commit -m "<Mensaje del commit>"`
6. Subir los cambios al repositio creado con el fork: `git push origin <rama que se uso>`
7. Crear un merge request al proyecto original.

## **English**

### Description

Background structure of the project intelligent system for tourists (Pandar) that aims to provide the routes, data and other means of connection to achieve the functionality of the Frontend of the application.

### Documentation

#### Technologies

|Tecnology                                                             |Version        | Benefits to the project
|:----------------------------------------------------------------------|:--------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|<img src="https://img.shields.io/badge/React-blue">             |16.13.1        | Greater overall mastery in the team, allows us to use JavaScript to build the fronend of a web application and huge community to receive support in the projects |
|<img src="https://img.shields.io/badge/Next.js-black">                    |9.5.2         | Allow us to build react projects with a standar structure wich give us better performance and speed in the development process |
|<img src="https://img.shields.io/badge/Bootstrap-purple">              |5.0.0-alpha1          | Give us a bunch of components already created in css wich improve the speed of the site layout construction |
|<img src="https://img.shields.io/badge/Typescript-darkblue">          |^3.9.7        | TypeScript saves us time catching errors and providing fixes before running the code |
|<img src="https://img.shields.io/badge/Sass-pink">                  |1.26.10           | Give us the ability to code css in a organize way and with better options |

#### Frontend

|Member            |Role                 |Tasks            |
|:------------------|:-------------------|:-----------------|
|Didier Cuetia   |**Frontend**  | In charge of the design, creation and integrations with the backend of the 404, Register and Login views |
|Oscar Pérez    |**Frontend**         | In charge of the design, creation and integrations with the backend of the Home, Search and Recommedations views |
|Santiago Arcila       |**Frontend**         | In charge of the design, creation and integrations with the backend of the Recover Password, Site Detail and Favorites views |
|Everyone              |**Everyone**           | Manual Tests and extra components |

#### Links of Interest

|Name                      |Link|
|:---------------------------|:------------|
|Meetings Videos     |<https://drive.google.com/drive/folders/1w8EpKRT_nDAw7tfnhyzEgh_CbkFCXN6l> |
|Figma Base Design          |<https://www.figma.com/file/vL1HShMkkVo1A9IcVoVrgB/Pandar?node-id=0%3A1> |
|Project Repository          |<https://gitlab.com/pandevs/frontend-intelligent-tourism-system> |
|Pandar website                 |<https://pandar.vercel.app> |

### Steps to Collaborate

1. Fork the project: by clicking the fork button (located at the top right)
2. Clone the project: `git clone https://gitlab.com/<your username>/backend-intellegent-tourism-system.git`
3. Install dependencies: `npm install` or use `npm i`
4. Run the project: `npm run dev`
5. Do the necessary improvements and make a commit: `git commit -am "<commit message>"` or use `git add .` and then `git commit -m "<commit message>"`
6. Upload changes to the repository created with the fork: `git push origin <rama que se uso>`
7. Create a merge request to the original project.
