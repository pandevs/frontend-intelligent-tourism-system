import React, { useContext, useState } from 'react';
import { TouristicSite } from '@models/TouristicSite';

const FavsContext = React.createContext({
  favs: [], toggleFav: (fav: TouristicSite) => { },
});

export const useFavsContext = () => useContext(FavsContext);

export const FavsContextProvider = ({ children }) => {
  const [favs, _setFavs] = useState([]);

  const toggleFav = (fav: TouristicSite) => {
    const found = favs.find((item) => item._id === fav._id);

    if (found) {
      _setFavs(favs.filter((item) => item._id !== fav._id));
    } else {
      delete fav.weight;
      _setFavs([...favs, fav]);
    }
  };

  return (
    <FavsContext.Provider value={{
      favs, toggleFav,
    }}
    >
      {children}
    </FavsContext.Provider>
  );
};
