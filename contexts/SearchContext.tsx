import React, { useContext, useState } from 'react';
import categories from '@services/categories';

const SearchContext = React.createContext({
  location: '',
  categories: [],
  selectedCategories: [categories[0].name],
  toggleCategory: (category: string) => {},
  setLocation: (location: string) => {},
});

export function useSearchContext() {
  return useContext(SearchContext);
}

export function SearchContextProvider({ children }) {
  const [location, _setLocation] = useState('');
  const [selectedCategories, setCategories] = useState<string[]>([categories[0].name]);

  const toggleCategory = (categoryName: string) => {
    if (selectedCategories.includes(categoryName)) {
      setCategories(selectedCategories.filter((f) => f !== categoryName));
    } else {
      setCategories([...selectedCategories, categoryName]);
    }
  };

  const setLocation = (_location: string) => {
    _setLocation(_location);
  };

  return (
    <SearchContext.Provider value={{
      categories, selectedCategories, toggleCategory, location, setLocation,
    }}
    >
      {children}
    </SearchContext.Provider>
  );
}
