import React, { useContext, useState } from 'react';
import { TouristicSite } from 'models/TouristicSite';
import dummySites from '@services/sites';
import slugify from '@services/slugify';

const touristicSiteContext = React.createContext({
  touristicSites: [],
  loading: false,
  setTouristicSites: (touristicSites: TouristicSite[]) => {},
  setLoading: (loading: boolean) => {},
  findSiteBySlug: (slug: string | string[]) => dummySites[0],
});

export const useTouristicSiteContext = () => useContext(touristicSiteContext);

export const TouristicSiteContextProvider = ({ children }) => {
  const [loading, _setLoading] = useState(false);
  const [touristicSites, _setTouristicSites] = useState<TouristicSite[]>([]);

  const setTouristicSites = (_touristicSites: TouristicSite[]) => {
    _setTouristicSites(_touristicSites);
  };

  const setLoading = (_loading: boolean) => {
    _setLoading(_loading);
  };

  const findSiteBySlug = (slug: string | string[]) => touristicSites.find((site) => slugify(site.location_name) === slug);

  return (
    <touristicSiteContext.Provider value={{
      touristicSites, loading, setTouristicSites, setLoading, findSiteBySlug,
    }}
    >
      {children}
    </touristicSiteContext.Provider>
  );
};
