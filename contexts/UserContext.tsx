import React, { useContext, useState } from 'react';
import authService from '@services/auth';
import { User } from 'models/User';
import auth from '@services/auth';

const UserContext = React.createContext({
  user: null,
  token: null,
  setUser: (user: any) => {},
  setToken: (token: string) => {},
});

export const useUserContext = () => useContext(UserContext);

export const UserContextProvider = ({ children }) => {
  const [user, _setUser] = useState<User>(null);
  const [token, _setToken] = useState('');

  const setUser = (_user: User) => {
    _setUser(_user);
  };

  const setToken = (_token: string) => {
    _setToken(_token);
  };

  return (
    <UserContext.Provider value={{
      user, token, setUser, setToken,
    }}
    >
      {children}
    </UserContext.Provider>
  );
};
