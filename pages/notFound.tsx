import React from "react";
import styles from "../styles/pages/notFound.module.scss";

const notFound = () => {
	return (
		<div className={styles.background}>
			<div className={styles.leftColumn}>
				<img src="/img/icon404.png" alt="" />
			</div>
			<div className={styles.rightColumn}>
				<h1>Ops!</h1>
				<p>Parece que no encontramos lo que estabas buscando</p>
				<button className="btn btn-primary">Volver al inicio:</button>
			</div>
		</div>
	);
};

export default notFound;
