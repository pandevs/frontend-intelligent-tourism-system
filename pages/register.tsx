import React from 'react';
import { BiUserCircle } from 'react-icons/bi';
import { AiOutlineMail } from 'react-icons/ai';
import { RiLockPasswordLine } from 'react-icons/ri';
import Greeting from '@components/Greeting/Greeting';
import styles from '../styles/pages/Register.module.scss';
import RegisterForm from '../components/RegisterForm/RegisterForm'
const Register = () => (
  <div className={styles.registerContainer}>
    <RegisterForm />
    <div className={styles.imagesContainer}>
      <Greeting
        title="Hola, nuevo amigo"
        content1="Ingresa para comenzar"
        content2="descubir sitios increibles"
        btnText="Inicia Sesión!"
        path="login"
      />
    </div>
  </div>
);

export default Register;
