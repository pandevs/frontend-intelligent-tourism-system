import React from 'react';
import { AppProps } from 'next/app';
import { UserContextProvider } from '@contexts/UserContext';
import { TouristicSiteContextProvider } from '@contexts/TouristicSiteContext';
import { SearchContextProvider } from '@contexts/SearchContext';
import { FavsContextProvider } from '@contexts/FavsContext';
import Layout from '@components/Layout/Layout';
import '../styles/globals.scss';

const MyApp = ({ Component, pageProps }: AppProps) => (
  <UserContextProvider>
    <FavsContextProvider>
      <SearchContextProvider>
        <TouristicSiteContextProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </TouristicSiteContextProvider>
      </SearchContextProvider>
    </FavsContextProvider>
  </UserContextProvider>
);

export default MyApp;
