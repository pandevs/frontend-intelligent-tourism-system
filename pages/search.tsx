import React from 'react';
import GeneralSearch from '@components/GeneralSearch/GeneralSearch';
import SearchResults from '@components/SearchResults/SearchResults';
import { useTouristicSiteContext } from '@contexts/TouristicSiteContext';
import styles from '../styles/pages/Search.module.scss';

const Search = () => {
  const { touristicSites, loading } = useTouristicSiteContext();

  return (
    <div className={styles.Search}>
      <div className="container">
        <GeneralSearch />
        <SearchResults touristicSites={touristicSites} loading={loading} didSearch />
      </div>
    </div>
  );
};

export default Search;
