import React from 'react';
import Greeting from '@components/Greeting/Greeting';
import LoginForm from '@components/LoginForm/LoginForm';
import styles from '../styles/pages/Login.module.scss';

const Register = () => (
  <div className={styles.loginContainer}>
    <div className={styles.imagesContainer}>
      <Greeting
        title="Bienvenido de vuelta"
        content1="Logueate con tu cuenta en Pandar"
        content2="para continuar con este viaje"
        btnText="Registrate"
        path="/register"
      />
    </div>
    <LoginForm />
  </div>
);

export default Register;
