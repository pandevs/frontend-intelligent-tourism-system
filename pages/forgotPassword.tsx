import React, { Fragment } from "react"
import styles from "../styles/pages/forgotPassword.module.scss"
import { FcKey } from 'react-icons/fc'

const forgotPassword = () => {
  return (
    <Fragment>
      <div className={styles.forgot_container}>

        <div className={styles.form}>
          <FcKey className={styles.iconContainer} />
          <h1>Recuperar contraseña</h1>
          <input type="email" placeholder="Email" className="form-control" />
          <button className="btn btn-primary">Enviar</button>
        </div>

        <svg
          id="Wave"
          data-name="Layer 1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 1299 201.44"
        >
          <defs></defs>
          <path
            className="cls-1"
            d="M.5,280.5s195-267,509-155,541,227,790,29v145H.5Z"
            transform="translate(-0.5 -98.06)"
          />
        </svg>
      </div>
    </Fragment>
  )
}

export default forgotPassword
