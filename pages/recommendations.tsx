import React, { useState } from 'react';
import suggestions from '@services/suggestions';
import AutosuggestLocation from '@components/AutosuggestLocation/AutosuggestLocation';
import Filters from '@components/Filters/Filters';
import SearchInput from '@components/SearchInput/SearchInput';
import categories from '@services/categories';
import SearchResults from '@components/SearchResults/SearchResults';
import useRecommendTouristicSites from '@hooks/useRecommendTouristicSites';
import styles from '../styles/pages/recommendations.module.scss';

const Recommendations = () => {
  const [budget, setBudget] = useState('');
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [location, setLocation] = useState('');
  const [myBudget, setMyBudget] = useState('');
  const [mySelectedCategories, setMySelectedCategories] = useState([]);
  const [myLocation, setMyLocation] = useState('');
  const [didSearch, setDidSearch] = useState(false);
  const [sites, loading] = useRecommendTouristicSites(myLocation, mySelectedCategories, myBudget);

  const searchData = () => {
    setDidSearch(true);
    setMyBudget(budget);
    setMyLocation(location);
    setMySelectedCategories(selectedCategories);
  };

  const toggleCategory = (categoryName: string) => {
    if (selectedCategories.includes(categoryName)) {
      setSelectedCategories(selectedCategories.filter((f) => f !== categoryName));
    } else {
      setSelectedCategories([...selectedCategories, categoryName]);
    }
  };

  return (
    <div className={styles.Recommendations}>
      <div className="container">
        <h1 className={styles.Recommendations__title}>Te recomendamos tu destino ideal</h1>

        <div className={styles.Recommendations_wrapper}>
          <div className={styles.Recommendations_wrapper_right}>
            <AutosuggestLocation value={location} setValue={setLocation} data={suggestions} />
          </div>
          <div className={styles.Recommendations_wrapper_center}>
            <SearchInput value={budget} setValue={setBudget} />
          </div>
          <div className={styles.Recommendations_wrapper_left}>
            <button disabled={!!loading} onClick={searchData} type="button" className="btn btn-secondary">Buscar</button>
          </div>
        </div>

        <Filters
          categories={categories}
          selectedCategories={selectedCategories}
          toggleCategory={toggleCategory}
        />

        <SearchResults touristicSites={sites} loading={loading} didSearch={didSearch} />
      </div>
    </div>

  );
};

export default Recommendations;
