import React from 'react';
import GeneralSearch from '@components/GeneralSearch/GeneralSearch';
import styles from '../styles/pages/Home.module.scss';

const Home = () => (
  <div className={`${styles.Home}`}>
    <div className="container">
      <h1 className={styles.Home__title}>
        La pandemia no durará para siempre, empieza a explorar...
      </h1>

      <GeneralSearch />
    </div>
  </div>
);

export default Home;
