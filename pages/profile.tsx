import React, { useState, useEffect } from 'react';
import TouristicSiteCard from '@components/TuristicSite/TuristicSite';
import { useFavsContext } from '@contexts/FavsContext';
import { GrEdit } from 'react-icons/gr';
import { User } from '@models/User';
import styles from '../styles/pages/profileFavs.module.scss';
import auth from '../services/auth';

const profileFavs = () => {
  const [user, setUser] = useState<User>(null);
  useEffect(() => {
    const sessionStorage = auth.getUser();
    setUser(sessionStorage);
  }, []);

  const { favs } = useFavsContext();
  return (
    <div className={`${styles.profileFavs} row`}>
      <div className={`${styles.profile} col`}>
        <div className={`${styles.profile__content}`}>
          <h2>Tu Perfil</h2>
          <img src="./img/logo.png" alt="Foto de Perfil" />
          <GrEdit size="2em" />
          <h3>
            {user?.first_name}
            {' '}
            {user?.last_name}
          </h3>
          <h3>{user?.email}</h3>
          {user?.country === 'colombia' && (
          <h3>
            {' '}
            {`${user?.country} 🇨🇴`}
            {' '}
          </h3>
          )}
          {user?.country === 'mexico' && (
          <h3>
            {' '}
            {`${user?.country} 🇲🇽`}
            {' '}
          </h3>
          )}
          <h3>{user?.city}</h3>
        </div>
      </div>
      <div className={`${styles.favs} col`}>
        <h2 className={styles.favsTitle}>Favoritos</h2>
        <div className={styles.favsItems}>
          {favs.length ? (
					  favs.map((site, index) => (
  						<TouristicSiteCard pos={index} site={site} key={site.location_name} />
					  ))
          ) : (
            <h3>Aun no tienes sitios favoritos</h3>
          )}
        </div>
      </div>
    </div>
  );
};

export default profileFavs;
