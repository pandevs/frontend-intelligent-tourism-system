import React from "react";
import { useRouter } from "next/router";
import Map from "@components/Map/Map";
import Link from "next/link";
import { useTouristicSiteContext } from "@contexts/TouristicSiteContext";
import ReactStars from "react-rating-stars-component";
import styles from "../../styles/pages/detailTouristicSite.module.scss";
import { BiPhoneCall } from "react-icons/bi";
import { BiWorld } from "react-icons/bi";
import { IoLogoUsd } from "react-icons/io";

const detailTouristicSite = () => {
	const router = useRouter();
	const { findSiteBySlug } = useTouristicSiteContext();
	const site = findSiteBySlug(router.query.name);
	const image = site
		? site.image.medium
			? site.image.medium.url
			: site.image
		: "https://static.dribbble.com/users/58267/screenshots/4257727/sans-store.jpg";

	console.log(site);

	return (
		<div className="container h-100">
			<div className={`${styles.detail} row`}>
				<div className={`${styles.detail__info} col-lg-4 col-sm-12`}>
					<h1>{site?.location_name}</h1>
					<h3>{site?.categories.join(",")}</h3>
					<div className={`d-flex ${styles.detailIcons}`}>
						<h3><BiPhoneCall size="1.5em" />{site?.phone}</h3>
						<a href={site?.web} target="_blank">
							<h3><BiWorld size="1.5em" />Sitio web</h3>
						</a>
						{site?.average_price === "0" && <h3><IoLogoUsd size="1.3em" />Gratis!</h3>}
						{site?.average_price !== "0" && <h3><IoLogoUsd size="1.3em" /> {site?.average_price} USD</h3>}
					</div>
					<div className="rating">
						<ReactStars
							count={5}
							size={24}
							value={Number(site?.rating)}
							edit={false}
							activeColor="#ffd700"
						/>
					</div>
					<h2>Acerca:</h2>
					<p>{site?.address}</p>
					<div className={styles.mapContainer}>
						<Map lat={site?.latitude} lng={site?.length} />
					</div>
					<small>
						LAT: {site?.latitude}, LONG: {site?.length}
					</small>
					<div className={styles.buttons_wrapper}>
						<button type="button" className="btn btn-primary">
							Añadir a favoritos
						</button>
						<Link href="/search">
							<button type="button" className="btn btn-secondary">
								Buscar más
							</button>
						</Link>
					</div>
				</div>
				<div className={`${styles.detail__image} col-lg-8 col-sm-12`}>
					<img
						className="img-fluid w-100"
						src={image}
						alt={site?.location_name}
					/>
				</div>
			</div>
		</div>
	);
};
export default detailTouristicSite;
