import { User } from '@models/User';

const USER_KEY = 'user';
const TOKEN_KEY = 'token';

const login = (user: User, token) => {
  sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  sessionStorage.setItem(TOKEN_KEY, token);
};

const logout = () => {
  sessionStorage.clear();
};

const getUser = () => {
  if (process.browser) {
    return JSON.parse(sessionStorage.getItem(USER_KEY));
  }
};

const getToken = () => {
  if (process.browser) {
    return sessionStorage.getItem(TOKEN_KEY);
  }
};

export default {
  login, logout, getUser, getToken,
};
