const slugify = (text: string) => text.toLowerCase()
  .replace(/[^\w\s-]/g, '') // remove non-word [a-z0-9_], non-whitespace, non-hyphen characters
  .replace(/[\s_-]+/g, '-') // swap any length of whitespace, underscore, hyphen characters with a single -
  .replace(/^-+|-+$/g, '');

export default slugify;
