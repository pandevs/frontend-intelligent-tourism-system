import {
  MdRestaurant, MdHotel, MdEventSeat, MdLocalBar,
} from 'react-icons/md';
import { RiGalleryFill } from 'react-icons/ri';
import {
  GiAncientRuins, GiBarracksTent, GiEcology, GiHighGrass,
} from 'react-icons/gi';
import { ImBooks } from 'react-icons/im';
import {
  FaMonument, FaCross, FaShoppingBag, FaUmbrellaBeach,
} from 'react-icons/fa';
import { HiOutlineDotsVertical } from 'react-icons/hi';

export default [
  {
    Icon: MdRestaurant,
    name: 'Restaurantes',
  },
  {
    Icon: MdHotel,
    name: 'Hoteles',
  },
  {
    Icon: MdEventSeat,
    name: 'Eventos',
  },
  {
    Icon: RiGalleryFill,
    name: 'Cultura',
  },
  {
    Icon: GiAncientRuins,
    name: 'Zonas Arqueológicas',
  },
  {
    Icon: GiBarracksTent,
    name: 'Parques Temáticos',
  },
  // {
  //   Icon: GiEcology,
  //   name: 'Ecoturismo',
  // },
  {
    Icon: FaCross,
    name: 'Sitios Religiosos',
  },
  {
    Icon: FaMonument,
    name: 'Arquitectura',
  },
  {
    Icon: FaShoppingBag,
    name: 'Compras',
  },
  // {
  //   Icon: ImBooks,
  //   name: 'Universidades',
  // },
  {
    Icon: MdLocalBar,
    name: 'Bares y Discotecas',
  },
  {
    Icon: FaUmbrellaBeach,
    name: 'Playas',
  },
  {
    Icon: GiHighGrass,
    name: 'Aire Libre',
  },
  // {
  //   Icon: HiOutlineDotsVertical,
  //   name: 'Otros',
  // },
];
