const MEXICO = 'MX';
const COLOMBIA = 'CO';

export default [
  {
    name: 'Acapulco',
    country: MEXICO,
  },
  {
    name: 'Aguascalientes',
    country: MEXICO,
  },
  {
    name: 'Apodaca',
    country: MEXICO,
  },
  {
    name: 'Buenavista',
    country: MEXICO,
  },
  {
    name: 'Campeche',
    country: MEXICO,
  },
  {
    name: 'Cancún',
    country: MEXICO,
  },
  {
    name: 'Celaya',
    country: MEXICO,
  },
  {
    name: 'Chalco',
    country: MEXICO,
  },
  {
    name: 'Chetumal',
    country: MEXICO,
  },
  {
    name: 'Chicoloapan',
    country: MEXICO,
  },
  {
    name: 'Chihuahua',
    country: MEXICO,
  },
  {
    name: 'Chilpancingo',
    country: MEXICO,
  },
  {
    name: 'Chimalhuacán',
    country: MEXICO,
  },
  {
    name: 'Ciudad Acuña',
    country: MEXICO,
  },
  {
    name: 'Ciudad de México',
    country: MEXICO,
  },
  {
    name: 'Ciudad del Carmen',
    country: MEXICO,
  },
  {
    name: 'Ciudad López Mateos',
    country: MEXICO,
  },
  {
    name: 'Ciudad Madero',
    country: MEXICO,
  },
  {
    name: 'Ciudad Obregón',
    country: MEXICO,
  },
  {
    name: 'Ciudad Valles',
    country: MEXICO,
  },
  {
    name: 'Ciudad Victoria',
    country: MEXICO,
  },
  {
    name: 'Coatzacoalcos',
    country: MEXICO,
  },
  {
    name: 'Colima',
    country: MEXICO,
  },
  {
    name: 'Córdoba',
    country: MEXICO,
  },
  {
    name: 'Cuauhtémoc',
    country: MEXICO,
  },
  {
    name: 'Cuautitlán',
    country: MEXICO,
  },
  {
    name: 'Cuautitlán Izcalli',
    country: MEXICO,
  },
  {
    name: 'Cuernavaca',
    country: MEXICO,
  },
  {
    name: 'Cuautla',
    country: MEXICO,
  },
  {
    name: 'Culiacán',
    country: MEXICO,
  },
  {
    name: 'Delicias',
    country: MEXICO,
  },
  {
    name: 'Durango',
    country: MEXICO,
  },
  {
    name: 'Ecatepec',
    country: MEXICO,
  },
  {
    name: 'Ensenada',
    country: MEXICO,
  },
  {
    name: 'Fresnillo',
    country: MEXICO,
  },
  {
    name: 'General Escobedo',
    country: MEXICO,
  },
  {
    name: 'Gómez Palacio',
    country: MEXICO,
  },
  {
    name: 'Guadalajara',
    country: MEXICO,
  },
  {
    name: 'Guadalupe',
    country: MEXICO,
  },
  {
    name: 'Guaymas',
    country: MEXICO,
  },
  {
    name: 'Hermosillo',
    country: MEXICO,
  },
  {
    name: 'Hidalgo del Parral',
    country: MEXICO,
  },
  {
    name: 'Iguala',
    country: MEXICO,
  },
  {
    name: 'Irapuato',
    country: MEXICO,
  },
  {
    name: 'Ixtapaluca',
    country: MEXICO,
  },
  {
    name: 'Jiutepec',
    country: MEXICO,
  },
  {
    name: 'Juárez',
    country: MEXICO,
  },
  {
    name: 'La Paz',
    country: MEXICO,
  },
  {
    name: 'León',
    country: MEXICO,
  },
  {
    name: 'Los Mochis',
    country: MEXICO,
  },
  {
    name: 'Manzanillo',
    country: MEXICO,
  },
  {
    name: 'Matamoros',
    country: MEXICO,
  },
  {
    name: 'Mazatlán',
    country: MEXICO,
  },
  {
    name: 'Mérida',
    country: MEXICO,
  },
  {
    name: 'Mexicali',
    country: MEXICO,
  },
  {
    name: 'Minatitlán',
    country: MEXICO,
  },
  {
    name: 'Morelia',
    country: MEXICO,
  },
  {
    name: 'Miramar',
    country: MEXICO,
  },
  {
    name: 'Monclova',
    country: MEXICO,
  },
  {
    name: 'Monterrey',
    country: MEXICO,
  },
  {
    name: 'Naucalpan',
    country: MEXICO,
  },
  {
    name: 'Naucalpan de Juárez',
    country: MEXICO,
  },
  {
    name: 'Navojoa',
    country: MEXICO,
  },
  {
    name: 'Nezahualcóyotl',
    country: MEXICO,
  },
  {
    name: 'Nogales',
    country: MEXICO,
  },
  {
    name: 'Nuevo Laredo',
    country: MEXICO,
  },
  {
    name: 'Oaxaca de Juárez',
    country: MEXICO,
  },
  {
    name: 'Ojo de Agua',
    country: MEXICO,
  },
  {
    name: 'Orizaba',
    country: MEXICO,
  },
  {
    name: 'Pachuca',
    country: MEXICO,
  },
  {
    name: 'Piedras Negras',
    country: MEXICO,
  },
  {
    name: 'Playa del Carmen',
    country: MEXICO,
  },
  {
    name: 'Poza Rica de Hidalgo',
    country: MEXICO,
  },
  {
    name: 'Puebla',
    country: MEXICO,
  },
  {
    name: 'Puerto Vallarta',
    country: MEXICO,
  },
  {
    name: 'Querétaro',
    country: MEXICO,
  },
  {
    name: 'Reynosa',
    country: MEXICO,
  },
  {
    name: 'Salamanca',
    country: MEXICO,
  },
  {
    name: 'Saltillo',
    country: MEXICO,
  },
  {
    name: 'San Cristóbal de las Casas',
    country: MEXICO,
  },
  {
    name: 'San Francisco Coacalco',
    country: MEXICO,
  },
  {
    name: 'San Juan Bautista Tuxtepec',
    country: MEXICO,
  },
  {
    name: 'San Juan del Río',
    country: MEXICO,
  },
  {
    name: 'San Luis Potosí',
    country: MEXICO,
  },
  {
    name: 'San Luis Río Colorado',
    country: MEXICO,
  },
  {
    name: 'San Miguel de Allende',
    country: MEXICO,
  },
  {
    name: 'San Nicolás de los Garza',
    country: MEXICO,
  },
  {
    name: 'San Pablo de las Salinas',
    country: MEXICO,
  },
  {
    name: 'San Pedro Garza García',
    country: MEXICO,
  },
  {
    name: 'Santa Catarina',
    country: MEXICO,
  },
  {
    name: 'Soledad de Graciano Sánchez',
    country: MEXICO,
  },
  {
    name: 'Tampico',
    country: MEXICO,
  },
  {
    name: 'Tapachula',
    country: MEXICO,
  },
  {
    name: 'Taxco',
    country: MEXICO,
  },
  {
    name: 'Tehuacán',
    country: MEXICO,
  },
  {
    name: 'Tepexpan',
    country: MEXICO,
  },
  {
    name: 'Tepic',
    country: MEXICO,
  },
  {
    name: 'Texcoco de Mora',
    country: MEXICO,
  },
  {
    name: 'Tijuana',
    country: MEXICO,
  },
  {
    name: 'Tlalnepantla',
    country: MEXICO,
  },
  {
    name: 'Tlaquepaque',
    country: MEXICO,
  },
  {
    name: 'Toluca',
    country: MEXICO,
  },
  {
    name: 'Tonalá',
    country: MEXICO,
  },
  {
    name: 'Torreón',
    country: MEXICO,
  },
  {
    name: 'Tulancingo de Bravo',
    country: MEXICO,
  },
  {
    name: 'Tuxtla',
    country: MEXICO,
  },
  {
    name: 'Uruapan',
    country: MEXICO,
  },
  {
    name: 'Veracruz',
    country: MEXICO,
  },
  {
    name: 'Villa de Álvarez',
    country: MEXICO,
  },
  {
    name: 'Villa Nicolás Romero',
    country: MEXICO,
  },
  {
    name: 'Villahermosa',
    country: MEXICO,
  },
  {
    name: 'Xalapa Enriquez',
    country: MEXICO,
  },
  {
    name: 'Xico',
    country: MEXICO,
  },
  {
    name: 'Zacatecas',
    country: MEXICO,
  },
  {
    name: 'Zamora',
    country: MEXICO,
  },
  {
    name: 'Zapopan',
    country: MEXICO,
  },
  {
    name: 'Bogotá',
    country: COLOMBIA,
  },
  {
    name: 'Medellín',
    country: COLOMBIA,
  },
  {
    name: 'Cali',
    country: COLOMBIA,
  },
  {
    name: 'Barranquilla',
    country: COLOMBIA,
  },
  {
    name: 'Cartagena de Indias',
    country: COLOMBIA,
  },
  {
    name: 'Soacha',
    country: COLOMBIA,
  },
  {
    name: 'Cúcuta',
    country: COLOMBIA,
  },
  {
    name: 'Soledad',
    country: COLOMBIA,
  },
  {
    name: 'Bucaramanga',
    country: COLOMBIA,
  },
  {
    name: 'Bello',
    country: COLOMBIA,
  },
  {
    name: 'Villavicencio',
    country: COLOMBIA,
  },
  {
    name: 'Ibagué',
    country: COLOMBIA,
  },
  {
    name: 'Santa Marta',
    country: COLOMBIA,
  },
  {
    name: 'Valledupar',
    country: COLOMBIA,
  },
  {
    name: 'Manizales',
    country: COLOMBIA,
  },
  {
    name: 'Pereira',
    country: COLOMBIA,
  },
  {
    name: 'Montería',
    country: COLOMBIA,
  },
  {
    name: 'Neiva',
    country: COLOMBIA,
  },
  {
    name: 'Pasto',
    country: COLOMBIA,
  },
  {
    name: 'Armenia',
    country: COLOMBIA,
  },
];
