const BASE_URL = process.env.NEXT_PUBLIC_BACKEND_HOST;
const PROFILES_PATH = '/pokemon';
const AUTH_PATH = '';
const TOURISTIC_SITE_PATH = '/touristic-site';
const SEARCH_SITE_PATH = '/search-site';

async function callApi(endpoint, query = {}, options = {}) {
  options.headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  };

  const url = new URL(BASE_URL + endpoint);

  if (Object.keys(query).length) {
    const categories = query?.categories;
    delete query.categories;

    const params = new URLSearchParams(query);

    if (categories) {
      categories.forEach((type) => params.append('categories[]', type));
    }

    url.search = params;
  }

  const response = await fetch(url, options);
  const data = await response.json();

  return data;
}

const api = {
  auth: {
    login(email, password) {
      return callApi(
        `${AUTH_PATH}/login`,
        {},
        {
          method: 'POST',
          body: JSON.stringify({ email, password }),
        },
      );
    },
    register(data) {
      return callApi(
        `${AUTH_PATH}/register`,
        {},
        {
          method: 'POST',
          body: JSON.stringify(data),
        },
      );
    },
  },
  touristicSite: {
    search(query) {
      return callApi(TOURISTIC_SITE_PATH, query);
    },
  },
  searchSite: {
    search(query) {
      return callApi(SEARCH_SITE_PATH, query);
    },
    recommend(query) {
      return callApi(`${SEARCH_SITE_PATH}/recommendations`, query);
    },
  },
  user: {
    list(query) {
      return callApi(PROFILES_PATH, query);
    },
    create(profile) {
      return callApi(
        PROFILES_PATH,
        {},
        {
          method: 'POST',
          body: JSON.stringify(profile),
        },
      );
    },
    read(profileId) {
      return callApi(`${PROFILES_PATH}/${profileId}`);
    },
    update(profileId, updates) {
      return callApi(
        `${PROFILES_PATH}/${profileId}`,
        {},
        {
          method: 'PATCH',
          body: JSON.stringify(updates),
        },
      );
    },
    remove(profileId) {
      return callApi(
        `${PROFILES_PATH}/${profileId}`,
        {},
        {
          method: 'DELETE',
        },
      );
    },
  },
};

export default api;
