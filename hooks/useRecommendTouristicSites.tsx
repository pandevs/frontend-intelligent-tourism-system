import { useEffect, useState } from 'react';
import api from '../api';

const useRecommendTouristicSites = (location, categories, budget) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [sites, setSites] = useState([]);

  const fetchData = async () => {
    setLoading(true);
    if (location && categories && budget) {
      try {
        const query = { city: location, categories, budget };
        const result = await api.searchSite.recommend(query);
        setLoading(false);
        setSites(result.body);
      } catch (error) {
        console.log(error);
        setSites([]);
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, [location, categories, budget]);

  return [sites, loading];
};

export default useRecommendTouristicSites;
