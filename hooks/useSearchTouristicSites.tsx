import { useEffect } from 'react';
import { useTouristicSiteContext } from '@contexts/TouristicSiteContext';
import api from '../api';

const useSearchTouristicSites = (location, categories, path) => {
  const { setTouristicSites, setLoading, loading } = useTouristicSiteContext();

  const fetchData = async () => {
    setLoading(true);
    if (location && categories.length && path === '/search' && !loading) {
      try {
        const query = { city: location, categories };
        const result = await api.searchSite.search(query);
        setLoading(false);
        setTouristicSites(result.body.filter((site) => site.location_name));
      } catch (error) {
        console.log(error);
        setTouristicSites([]);
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, [location, categories.length, path]);

  return [loading];
};

export default useSearchTouristicSites;
